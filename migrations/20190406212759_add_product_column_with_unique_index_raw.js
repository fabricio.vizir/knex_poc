const tableName = 'products_raw';
const columnName = 'manufacturer_id';

exports.up = function(knex, Promise) {
   return knex.raw(`
ALTER TABLE ${tableName} ADD COLUMN IF NOT EXISTS ${columnName} INTEGER UNIQUE;
`);
};

exports.down = function(knex, Promise) {
    return knex.raw(`
ALTER TABLE ${tableName} DROP COLUMN IF EXISTS ${columnName} CASCADE;
`);
};
