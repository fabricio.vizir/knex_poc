const tableName = 'products_raw';
const columnName = 'serie_number';

exports.up = function(knex, Promise) {
   return knex.raw(`
ALTER TABLE ${tableName} ADD COLUMN IF NOT EXISTS ${columnName} INTEGER DEFAULT 1;
`);
};

exports.down = function(knex, Promise) {
    return knex.raw(`
ALTER TABLE ${tableName} DROP COLUMN IF EXISTS ${columnName};
`);
};
